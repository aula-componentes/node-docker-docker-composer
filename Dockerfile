FROM node:alpine

WORKDIR /user/app

COPY package*.json ./
RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm","start"];sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose